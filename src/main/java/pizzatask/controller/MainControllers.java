package pizzatask.controller;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pizzatask.controller.utils.Executable;
import pizzatask.controller.utils.TextPrepare;
import pizzatask.model.bakerie.Bakery;
import pizzatask.model.pizzaaddon.optional.Dough;
import pizzatask.model.pizzaaddon.optional.Sauce;
import pizzatask.model.pizzabase.Type;
import pizzatask.ui.element.ElementFabric;
import pizzatask.ui.element.bo.SceneBO;
import pizzatask.ui.utils.SettingsReader;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class MainControllers {
    private static Logger LOG = LogManager.getLogger(MainControllers.class);
    private static Map<String, Executable> bakerySelector = new LinkedHashMap<>();
    private static Map<String, Executable> pizzaTypeSelector = new LinkedHashMap<>();
    private static Map<String, Executable> sauceSelector = new LinkedHashMap<>();
    private static Map<String, Executable> doughSelector = new LinkedHashMap<>();
    private static Map<String, Boolean> checkBoxSelectedMap = new LinkedHashMap<>();
    private static Label toChange;

    static {
        List<String> bakeryList = SettingsReader.getBakeryList();
        List<String> pizzaTypeList = SettingsReader.getPizzaList();
        List<String> sauceList = SettingsReader.getSauceList();
        List<String> doughList = SettingsReader.getDoughList();
        bakerySelector.put(bakeryList.get(0), () -> PizzaPaymaster.selectBakery(Bakery.LVIV));
        bakerySelector.put(bakeryList.get(1), () -> PizzaPaymaster.selectBakery(Bakery.KYIV));
        bakerySelector.put(bakeryList.get(2), () -> PizzaPaymaster.selectBakery(Bakery.DNIPRO));
        pizzaTypeSelector.put(pizzaTypeList.get(0), () -> PizzaPaymaster.selectPizza(Type.CHEESE));
        pizzaTypeSelector.put(pizzaTypeList.get(1), () -> PizzaPaymaster.selectPizza(Type.ITALIAN));
        pizzaTypeSelector.put(pizzaTypeList.get(2), () -> PizzaPaymaster.selectPizza(Type.PEPPERONI));
        pizzaTypeSelector.put(pizzaTypeList.get(3), () -> PizzaPaymaster.selectPizza(Type.VEGETARIAN));
        sauceSelector.put(sauceList.get(0), () -> PizzaPaymaster.selectSauce(Sauce.MARINARA));
        sauceSelector.put(sauceList.get(1), () -> PizzaPaymaster.selectSauce(Sauce.TOMATO));
        sauceSelector.put(sauceList.get(2), () -> PizzaPaymaster.selectSauce(Sauce.PESTO));
        doughSelector.put(doughList.get(0), () -> PizzaPaymaster.selectDough(Dough.THIN));
        doughSelector.put(doughList.get(1), () -> PizzaPaymaster.selectDough(Dough.THICK));
    }

    private MainControllers() {
    }

    public static void selectBakery(SceneBO taskScene, int boxIndex) {
        String text = ((ChoiceBox) taskScene.getBox().getChildren().get(boxIndex)).getValue().toString();
        executeTask(bakerySelector.get(text));
    }

    public static void selectPizzaType(SceneBO taskScene, int boxIndex) {
        String text = ((ChoiceBox) taskScene.getBox().getChildren().get(boxIndex)).getValue().toString();
        executeTask(pizzaTypeSelector.get(text));
    }

    public static void selectSauce(SceneBO taskScene, int boxIndex) {
        String text = ((ChoiceBox) taskScene.getBox().getChildren().get(boxIndex)).getValue().toString();
        executeTask(sauceSelector.get(text));
    }

    public static void selectDough(SceneBO taskScene, int boxIndex) {
        String text = ((ChoiceBox) taskScene.getBox().getChildren().get(boxIndex)).getValue().toString();
        executeTask(doughSelector.get(text));
    }

    public static void checkBoxesChecking() {
        ElementFabric.getCheckBoxSet().forEach(checkBox -> checkBoxSelectedMap.put(checkBox.getText(), checkBox.isSelected()));
    }

    public static void cookPizza() {
        PizzaPaymaster.cookPizzaByOrder(checkBoxSelectedMap);
    }

    private static void executeTask(Executable task) {
        LOG.info("processing task...");
        task.execute();
    }


    public static void outputResult(String result) {

        toChange.setText(TextPrepare.processingTextBeforeOutput(result));
    }

    public static void setLabelForTasks(Label toChange) {
        MainControllers.toChange = toChange;
    }
}
