package pizzatask.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pizzatask.model.bakerie.AbstractBakerie;
import pizzatask.model.bakerie.Bakery;
import pizzatask.model.bakerie.impl.DniproBakerie;
import pizzatask.model.bakerie.impl.KyivBakerie;
import pizzatask.model.bakerie.impl.LvivBakerie;
import pizzatask.model.pizzaaddon.Wishes;
import pizzatask.model.pizzaaddon.optional.Dough;
import pizzatask.model.pizzaaddon.optional.Sauce;
import pizzatask.model.pizzaaddon.optional.Topping;
import pizzatask.model.pizzabase.Type;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class PizzaPaymaster {
    private static Logger LOG = LogManager.getLogger(PizzaPaymaster.class);
    private static AbstractBakerie selectedBakerie;
    private static Type selectedPizzaType;
    private static Sauce selectedSauce = Sauce.TOMATO;
    private static Dough selectedDough = Dough.THICK;
    private static List<Topping> toppingList;

    static void selectBakery(Bakery bakery) {
        if (bakery == Bakery.LVIV) {
            selectedBakerie = new LvivBakerie();
            LOG.info("Lviv bakerie was selected");
        } else if (bakery == Bakery.KYIV) {
            selectedBakerie = new KyivBakerie();
            LOG.info("Kyiv bakerie was selected");
        } else if (bakery == Bakery.DNIPRO) {
            selectedBakerie = new DniproBakerie();
            LOG.info("Dnipro bakerie was selected");
        }
    }

    static void selectPizza(Type pizzaType) {
        LOG.info(pizzaType.name() + " was selected");
        selectedPizzaType = pizzaType;
    }

    static void selectSauce(Sauce sauce) {
        LOG.info(sauce.name() + " was selected");
        selectedSauce = sauce;
    }

    static void selectDough(Dough dough) {
        LOG.info(dough.name() + " was selected");
        selectedDough = dough;
    }

    static void cookPizzaByOrder(Map<String, Boolean> toppingMap) {
        readToppings(toppingMap);
        if (!Optional.ofNullable(selectedBakerie).isPresent()) {
            MainControllers.outputResult("Please, select bakery first");
        } else if (!Optional.ofNullable(selectedPizzaType).isPresent()) {
            MainControllers.outputResult("Please, select Pizza Type");
        } else {
            MainControllers.outputResult(
                    selectedBakerie.cookPizza(selectedPizzaType,
                            new Wishes(selectedDough, selectedSauce, toppingList.stream().toArray(Topping[]::new))
                    ).toString());
        }

    }

    private static void readToppings(Map<String, Boolean> toppingMap) {
        toppingList = new LinkedList<>();
        toppingMap.forEach((topping, isSelected) -> {
            if (isSelected) {
                toppingList.add(Topping.getToppingByKey(topping));
            }
        });
    }

}
