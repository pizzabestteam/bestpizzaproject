package pizzatask.controller.utils;

@FunctionalInterface
public interface Executable {
    public void execute();
}
