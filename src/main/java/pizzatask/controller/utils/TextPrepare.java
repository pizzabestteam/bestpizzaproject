package pizzatask.controller.utils;

import pizzatask.ui.utils.SettingsManager;

public abstract class TextPrepare {
    public static String processingTextBeforeOutput(String text) {
        StringBuilder builder = new StringBuilder(text);
        StringBuilder bufStringBuilder = new StringBuilder();
        if (text.length() < Integer.parseInt(SettingsManager.getSetting("DEFAULT_TEXT_WIDTH"))){
            return text;
        }
        for (int k = 0, j = 0; k < text.length(); k++, j++) {
            if (text.charAt(k) == '\n') {
                j = 0;
            }
            if (j >= Integer.parseInt(SettingsManager.getSetting("DEFAULT_TEXT_WIDTH"))) {
                bufStringBuilder = new StringBuilder(builder.substring(0, k) + '\n' + builder.substring(k, builder.length()));
                builder = new StringBuilder(bufStringBuilder);
                j = 0;
            }
        }
        return bufStringBuilder.toString();
    }
}
