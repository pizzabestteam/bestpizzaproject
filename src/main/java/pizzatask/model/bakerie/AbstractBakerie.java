package pizzatask.model.bakerie;

import pizzatask.model.pizzaaddon.Wishes;
import pizzatask.model.pizzabase.Pizza;
import pizzatask.model.pizzabase.Type;

public abstract class AbstractBakerie {
    protected abstract Pizza createPizza(Type pizzaType, Wishes clientWishes);

    public Pizza cookPizza(Type type, Wishes wishes) {
        Pizza pizza = createPizza(type, wishes);
        pizza.SaucePreparation();
        pizza.DoughKneading();
        pizza.ToppingsAdding();
        pizza.Baking();
        pizza.Box();
        return pizza;
    }

}
