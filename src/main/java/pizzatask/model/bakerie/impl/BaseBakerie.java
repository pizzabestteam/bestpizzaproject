package pizzatask.model.bakerie.impl;

import pizzatask.model.bakerie.AbstractBakerie;
import pizzatask.model.pizzaaddon.Wishes;
import pizzatask.model.pizzaaddon.optional.Topping;
import pizzatask.model.pizzabase.Pizza;
import pizzatask.model.pizzabase.Type;
import pizzatask.model.pizzabase.impl.Cheese;
import pizzatask.model.pizzabase.impl.Italian;
import pizzatask.model.pizzabase.impl.Pepperoni;
import pizzatask.model.pizzabase.impl.Vegetarian;

abstract class BaseBakerie extends AbstractBakerie {
    private Pizza pizza;

    Pizza createPizza(Type pizzaType, Wishes clientWishes, Topping extraTopping) {
        identifyPizza(pizzaType);
        clientWishes.addExtraTopping(extraTopping);
        pizza.addWishes(clientWishes);
        return pizza;
    }

    private void identifyPizza(Type pizzaType) {
        if (pizzaType == Type.CHEESE) {
            pizza = new Cheese();
        } else if (pizzaType == Type.ITALIAN) {
            pizza = new Italian();
        } else if (pizzaType == Type.PEPPERONI) {
            pizza = new Pepperoni();
        } else {
            pizza = new Vegetarian();
        }
    }
}
