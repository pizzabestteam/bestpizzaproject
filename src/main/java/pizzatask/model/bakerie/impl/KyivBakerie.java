package pizzatask.model.bakerie.impl;

import pizzatask.model.pizzaaddon.Wishes;
import pizzatask.model.pizzaaddon.optional.Topping;
import pizzatask.model.pizzabase.Pizza;
import pizzatask.model.pizzabase.Type;

public class KyivBakerie extends BaseBakerie {
    private static Topping extraToppingKyiv = Topping.SECRET_INGREDIENT;

    @Override
    protected Pizza createPizza(Type pizzaType, Wishes clientWishes) {
        return super.createPizza(pizzaType, clientWishes, extraToppingKyiv);
    }
}
