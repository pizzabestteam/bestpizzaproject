package pizzatask.model.pizzaaddon;

import com.sun.istack.internal.NotNull;
import pizzatask.model.pizzaaddon.optional.Dough;
import pizzatask.model.pizzaaddon.optional.Sauce;
import pizzatask.model.pizzaaddon.optional.Topping;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class Wishes {
    private Dough dough;
    private Sauce sauce;
    private Set<Topping> toppings;

    public Wishes(@NotNull Dough dough, @NotNull Sauce sauce, @NotNull Topping... toppings) {
        this.dough = dough;
        this.sauce = sauce;
        this.toppings = Arrays.stream(toppings).collect(Collectors.toSet());
    }

    public void addExtraTopping(Topping topping) {
        this.toppings.add(topping);
    }

    public Dough getDough() {
        return dough;
    }

    public Sauce getSauce() {
        return sauce;
    }

    public Set<Topping> getToppings() {
        return toppings;
    }

}
