package pizzatask.model.pizzaaddon.optional;

public enum Dough {
    THICK, THIN;
}
