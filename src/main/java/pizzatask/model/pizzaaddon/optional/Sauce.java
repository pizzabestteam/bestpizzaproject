package pizzatask.model.pizzaaddon.optional;

public enum Sauce {
    MARINARA, TOMATO, PESTO;
}
