package pizzatask.model.pizzaaddon.optional;

public enum Topping {
    SALAMI("salami"), BACON("bacon"), HAM("ham"),
    PARMESAN("parmesan"), FETA("feta"), PINEAPPLE("pineapple"),
    CHICKEN("chicken"), OLIVE("olive"), GRASS("grass"),
    SECRET_INGREDIENT("secret_ingredient");

    private String toppingName;

    Topping(String toppingName) {
        this.toppingName = toppingName;
    }

    public String getToppingName() {
        return toppingName;
    }

    public static Topping getToppingByKey(String key) {
        if (key.equals(SALAMI.getToppingName())) {
            return SALAMI;
        }
        if (key.equals(BACON.getToppingName())) {
            return BACON;
        }
        if (key.equals(HAM.getToppingName())) {
            return HAM;
        }
        if (key.equals(PARMESAN.getToppingName())) {
            return PARMESAN;
        }
        if (key.equals(FETA.getToppingName())) {
            return FETA;
        }
        if (key.equals(PINEAPPLE.getToppingName())) {
            return PINEAPPLE;
        }
        if (key.equals(CHICKEN.getToppingName())) {
            return CHICKEN;
        }
        if (key.equals(OLIVE.getToppingName())) {
            return OLIVE;
        }
        if (key.equals(GRASS.getToppingName())) {
            return GRASS;
        }
        return SECRET_INGREDIENT;
    }
}
