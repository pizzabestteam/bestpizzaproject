package pizzatask.model.pizzabase;

import pizzatask.model.pizzaaddon.Wishes;

public interface Pizza {
    void addWishes(Wishes wishes);

    void SaucePreparation();

    void DoughKneading();

    void ToppingsAdding();

    void Baking();

    void Box();
}
