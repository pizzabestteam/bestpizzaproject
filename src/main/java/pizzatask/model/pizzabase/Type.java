package pizzatask.model.pizzabase;

public enum Type {
    CHEESE, ITALIAN, PEPPERONI, VEGETARIAN;
}
