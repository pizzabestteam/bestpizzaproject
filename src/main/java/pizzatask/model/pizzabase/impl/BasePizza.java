package pizzatask.model.pizzabase.impl;

import pizzatask.model.pizzaaddon.Wishes;
import pizzatask.model.pizzabase.Pizza;

abstract class BasePizza implements Pizza {
    Wishes wishes;

    @Override
    public void addWishes(Wishes wishes) {
        this.wishes = wishes;
    }

    @Override
    public String toString() {
        return wishes.getDough().toString().toLowerCase() + " dough and " +
                wishes.getSauce().toString().toLowerCase() + " sauce and with " +
                wishes.getToppings().toString() + " toppings ";
    }
}
