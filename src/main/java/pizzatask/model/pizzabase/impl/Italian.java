package pizzatask.model.pizzabase.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pizzatask.model.pizzaaddon.Wishes;
import pizzatask.model.pizzaaddon.optional.Topping;

import java.util.Arrays;
import java.util.List;

public class Italian extends BasePizza {
    private static Logger LOG = LogManager.getLogger(Italian.class);
    private static List<Topping> italianDefaultToppingList = Arrays.asList(Topping.FETA, Topping.OLIVE);

    @Override
    public void addWishes(Wishes wishes) {
        italianDefaultToppingList.forEach(wishes::addExtraTopping);
        super.addWishes(wishes);
    }

    @Override
    public void SaucePreparation() {
        LOG.info("Sauce: " + wishes.getSauce());
    }

    @Override
    public void DoughKneading() {
        LOG.info("Dough: " + wishes.getDough());
    }

    @Override
    public void ToppingsAdding() {
        LOG.info("Adding toppings");
    }

    @Override
    public void Baking() {
        LOG.info("Baking your pizza, wait please...");
    }

    @Override
    public void Box() {
        LOG.info("Boxing your order.");
    }

    @Override
    public String toString() {
        return "Italian pizza with " +
                super.toString();
    }
}
