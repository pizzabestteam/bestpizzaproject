package pizzatask.ui;

import javafx.application.Application;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pizzatask.controller.MainControllers;
import pizzatask.ui.element.ElementFabric;
import pizzatask.ui.element.bo.SceneBO;
import pizzatask.ui.element.bo.StageBO;
import pizzatask.ui.utils.Language;
import pizzatask.ui.utils.SettingsManager;
import pizzatask.ui.utils.SettingsReader;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

public class View extends Application {
    private static Logger LOG = LogManager.getLogger(View.class);
    private StageBO mainWindow;
    private SceneBO mainMenuScene;
    private SceneBO taskScene;
    private SceneBO settingsScene;
    private SceneBO languageSelectionScene;
    private static ResourceBundle bundle;

    @Override
    public void start(Stage primaryStage) {
        LOG.info("Application starting...");
        mainWindow = new StageBO(primaryStage);
        this
                .setApplicationLanguage(SettingsManager.getDefaultApplicationLocale())
                .updateUserInterface()
                .showWindow();
    }

    private View updateUserInterface() {
        LOG.info("Updating GUI...");
        this
                .initializeAllScene()
                .initializeSceneElements();
        mainWindow
                .setTitle(bundle.getString("Title"))
                .setScene(mainMenuScene);
        return this;
    }

    private View initializeAllScene() {
        LOG.info("Initializing scenes...");
        mainMenuScene = new SceneBO();
        taskScene = new SceneBO();
        settingsScene = new SceneBO();
        languageSelectionScene = new SceneBO();
        return this;
    }

    private void initializeSceneElements() {
        LOG.info("Initializing scenes elements...");
        mainMenuScene.addAllElements(
                ElementFabric.createLabel("MainLabel"),
                ElementFabric.createCustomButton(event -> changeScene(taskScene), "OrderPizza"),
                ElementFabric.createCustomButton(event -> changeScene(settingsScene), "SettingsLabel"),
                ElementFabric.createExitButton()
        );
        taskScene.addAllElements(
                ElementFabric.createLabel("TaskLabel"),
                ElementFabric.createLabel("ChooseBakery"),
                ElementFabric.createChoiceBox(event -> MainControllers.selectBakery(taskScene, 2), SettingsReader.getBakeryList()),
                ElementFabric.createLabel("ChoosePizzaType"),
                ElementFabric.createChoiceBox(event -> MainControllers.selectPizzaType(taskScene, 4), SettingsReader.getPizzaList()),
                ElementFabric.createLabel("ChooseSauce"),
                ElementFabric.createChoiceBox(event -> MainControllers.selectSauce(taskScene, 6), SettingsReader.getSauceList()),
                ElementFabric.createLabel("ChooseDough"),
                ElementFabric.createChoiceBox(event -> MainControllers.selectDough(taskScene, 8), SettingsReader.getDoughList()),
                //---CheckBoxes
                ElementFabric.createCheckBox("salami"),
                ElementFabric.createCheckBox("bacon"),
                ElementFabric.createCheckBox("ham"),
                ElementFabric.createCheckBox("parmesan"),
                ElementFabric.createCheckBox("feta"),
                ElementFabric.createCheckBox("pineapple"),
                ElementFabric.createCheckBox("chicken"),
                ElementFabric.createCheckBox("olive"),
                ElementFabric.createCheckBox("grass"),
                ElementFabric.createCheckBox("secret_ingredient"),
                //---
                ElementFabric.createCustomButton(event -> MainControllers.cookPizza(), "Order"),
                ElementFabric.createCustomButton(event -> changeScene(mainMenuScene), "Back")
        );
        MainControllers.setLabelForTasks(((Label) taskScene.getBox().getChildren().get(1)));
        settingsScene.addAllElements(
                ElementFabric.createLabel("SettingsLabel"),
                ElementFabric.createCustomButton(event -> changeScene(languageSelectionScene), "LanguageLabel"),
                ElementFabric.createCustomButton(event -> changeApplicationTheme(), "ChangeTheme"),
                ElementFabric.createCustomButton(event -> changeScene(mainMenuScene), "Back")
        );
        languageSelectionScene.addAllElements(
                ElementFabric.createLabel("LanguageLabel"),
                ElementFabric.createCustomButton(event -> setApplicationLanguage(Language.UK), "Ukr"),
                ElementFabric.createCustomButton(event -> setApplicationLanguage(Language.EN), "Eng"),
                ElementFabric.createCustomButton(event -> setApplicationLanguage(Language.PL), "Pl"),
                ElementFabric.createCustomButton(event -> changeScene(settingsScene), "Back")
        );
    }

    private View setApplicationLanguage(Language language) {
        LOG.info("Setting up application language.");
        initializeBundle(language);
        if (Optional.ofNullable(mainMenuScene).isPresent()) {
            updateUserInterface();
        }
        return this;
    }

    private void initializeBundle(Language language) {
        bundle = ResourceBundle.getBundle("menu", new Locale(language.getLanguage()));
        ElementFabric.updateLanguage();
        SettingsReader.updateLanguage();
    }


    private void changeApplicationTheme() {
        LOG.info("Application theme was changed.");
        SettingsManager.changeTheme();
        if (Optional.ofNullable(mainMenuScene).isPresent()) {
            updateUserInterface();
        }
    }

    private void changeScene(SceneBO scene) {
        mainWindow.setScene(scene);
    }

    private void showWindow() {
        mainWindow.show();
    }

    public static ResourceBundle getCurrentBundle() {
        return bundle;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
