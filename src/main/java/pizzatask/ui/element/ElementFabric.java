package pizzatask.ui.element;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import pizzatask.controller.MainControllers;
import pizzatask.ui.View;
import pizzatask.ui.utils.SettingsManager;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

public abstract class ElementFabric {
    private static ResourceBundle bundle;
    private static Set<CheckBox> checkBoxSet = new LinkedHashSet<>();

    public static void updateLanguage() {
        bundle = View.getCurrentBundle();
    }

    public static Button createExitButton() {
        Button exitButton = new Button(bundle.getString("Exit"));
        exitButton.setStyle(SettingsManager.getCurrentButtonStyle());
        exitButton.setOnAction(e -> System.exit(0));
        return exitButton;
    }

    public static Label createLabel(String key) {
        Label label = new Label(bundle.getString(key));
        label.setStyle(SettingsManager.getCurrentTextStyle());
        return label;
    }

    public static Label createTextLabel(String text) {
        Label label = new Label(text);
        label.setStyle(SettingsManager.getCurrentTextStyle());
        return label;
    }

    public static Button createCustomButton(EventHandler<ActionEvent> value, String key) {
        Button button = new Button(bundle.getString(key));
        button.setStyle(SettingsManager.getCurrentButtonStyle());
        button.setOnAction(value);
        return button;
    }

    public static CheckBox createCheckBox(String key) {
        CheckBox checkBox = new CheckBox(bundle.getString(key));
        checkBox.setOnAction(event -> MainControllers.checkBoxesChecking());
        checkBox.setStyle(SettingsManager.getCurrentTextStyle());
        checkBoxSet.add(checkBox);
        return checkBox;
    }

    public static ChoiceBox<? extends java.io.Serializable> createChoiceBox(EventHandler<ActionEvent> value, List<String> variants) {
        ChoiceBox<java.io.Serializable> box = new ChoiceBox<>();
        box.getItems().addAll(variants);
        box.setOnAction(value);
        box.setStyle(SettingsManager.getCurrentTextStyle());
        return box;
    }

    public static Set<CheckBox> getCheckBoxSet() {
        return checkBoxSet;
    }

}
