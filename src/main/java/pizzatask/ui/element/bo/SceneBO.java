package pizzatask.ui.element.bo;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.layout.VBox;
import pizzatask.ui.utils.SettingsManager;


public class SceneBO {
    private Scene scene;
    private VBox box;

    public SceneBO() {
        box = new VBox(Integer.parseInt(SettingsManager.getSetting("DEFAULT_BUTTON_SPACING")));
        box.setStyle(SettingsManager.getCurrentBackgroundStyle());
        box.setAlignment(Pos.CENTER);
        scene = new Scene(box, Integer.parseInt(SettingsManager.getSetting("DEFAULT_WIDTH"))
                , Integer.parseInt(SettingsManager.getSetting("DEFAULT_HEIGHT")));
    }

    public Scene getScene() {
        return scene;
    }

    public VBox getBox() {
        return box;
    }

    public void addAllElements(Control... elements) {
        box.getChildren().addAll(elements);
    }
}
