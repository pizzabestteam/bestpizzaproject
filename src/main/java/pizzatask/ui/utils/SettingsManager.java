package pizzatask.ui.utils;

import java.util.ResourceBundle;

public abstract class SettingsManager {
    private static ResourceBundle bundle = ResourceBundle.getBundle("appsettings");
    private static Language DEFAULT_APPLICATION_LOCALE = Language.EN;
    private static String CURRENT_BACKGROUND_STYLE = bundle.getString("DEFAULT_BACKGROUND_STYLE");
    private static String CURRENT_BUTTON_STYLE = bundle.getString("DEFAULT_BUTTON_STYLE");
    private static String CURRENT_TEXT_STYLE = bundle.getString("DEFAULT_TEXT_STYLE");

    public static void changeTheme() {
        if (CURRENT_BACKGROUND_STYLE.equals(bundle.getString("DEFAULT_BACKGROUND_STYLE"))) {
            CURRENT_BACKGROUND_STYLE = bundle.getString("DARK_BACKGROUND_STYLE");
            CURRENT_BUTTON_STYLE = bundle.getString("DARK_BUTTON_STYLE");
            CURRENT_TEXT_STYLE = bundle.getString("DARK_TEXT_STYLE");
        } else {
            CURRENT_BACKGROUND_STYLE = bundle.getString("DEFAULT_BACKGROUND_STYLE");
            CURRENT_BUTTON_STYLE = bundle.getString("DEFAULT_BUTTON_STYLE");
            CURRENT_TEXT_STYLE = bundle.getString("DEFAULT_TEXT_STYLE");
        }
    }

    public static String getSetting(String key) {
        return bundle.getString(key);
    }

    public static String getCurrentBackgroundStyle() {
        return CURRENT_BACKGROUND_STYLE;
    }

    public static String getCurrentButtonStyle() {
        return CURRENT_BUTTON_STYLE;
    }

    public static String getCurrentTextStyle() {
        return CURRENT_TEXT_STYLE;
    }

    public static Language getDefaultApplicationLocale() {
        return DEFAULT_APPLICATION_LOCALE;
    }
}
