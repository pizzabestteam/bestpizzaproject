package pizzatask.ui.utils;

import pizzatask.ui.View;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public abstract class SettingsReader {
    private static List<String> bakeriesKeys;
    private static List<String> pizzaKeys;
    private static List<String> sauceKeys;
    private static List<String> doughKeys;
    private static ResourceBundle bundle;

    static {
        bakeriesKeys = new LinkedList<>();
        pizzaKeys = new LinkedList<>();
        sauceKeys = new LinkedList<>();
        doughKeys = new LinkedList<>();
        bakeriesKeys.addAll(Arrays.asList(
                "LvivBakery",
                "KyivBakerie",
                "DniproBakerie"
        ));
        pizzaKeys.addAll(Arrays.asList(
                "Cheese",
                "Italian",
                "Pepperoni",
                "Vegetarian"
        ));
        sauceKeys.addAll(Arrays.asList(
                "MARINARA",
                "TOMATO",
                "PESTO"
        ));
        doughKeys.addAll(Arrays.asList(
                "THICK",
                "THIN"
        ));
    }

    public static List<String> getPizzaList() {
        List<String> pizzaList = new LinkedList<>();
        pizzaKeys.forEach(key -> pizzaList.add(bundle.getString(key)));
        return pizzaList;
    }

    public static List<String> getBakeryList() {
        List<String> bakeryList = new LinkedList<>();
        bakeriesKeys.forEach(key -> bakeryList.add(bundle.getString(key)));
        return bakeryList;
    }

    public static List<String> getSauceList() {
        List<String> sauceList = new LinkedList<>();
        sauceKeys.forEach(key -> sauceList.add(bundle.getString(key)));
        return sauceList;
    }

    public static List<String> getDoughList() {
        List<String> doughList = new LinkedList<>();
        doughKeys.forEach(key -> doughList.add(bundle.getString(key)));
        return doughList;
    }

    public static void updateLanguage() {
        bundle = View.getCurrentBundle();
    }
}
